//
//  AppDelegate.h
//  MyAssessment
//
//  Created by Raymond Brion on 13/04/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

