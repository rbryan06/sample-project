//
//  SecondViewController.m
//  MyAssessment
//
//  Created by Raymond Brion on 13/04/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import "SecondViewController.h"
#import "ViewController.h"

static NSString* kBackToFirstViewControllerSegueId
  = @"backToFirstViewController";

@interface SecondViewController ()

@property (nonatomic, strong) NSString* tappedButtonText;

@end

@implementation SecondViewController

#pragma mark - View lifecycle

- (void) viewDidLoad
{
  [super viewDidLoad];
  
  self.tappedButtonText = nil;
}

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];
  
  self.view.backgroundColor = self.backgroundColor;
}

#pragma mark - Navigation methods

- (void) prepareForSegue: (UIStoryboardSegue*) segue
                  sender: (id)                 sender
{
  if ([segue.identifier isEqualToString: kBackToFirstViewControllerSegueId])
  {
    ViewController* viewController
      = (ViewController*) segue.destinationViewController;
    
    viewController.tappedButtonText = self.tappedButtonText;
  }
}

#pragma mark - IBAction methods

- (IBAction) buttonTapped: (id) sender
{
  /* Determine the text based on the button's tag */
  UIButton* button = (UIButton*) sender;
  switch (button.tag)
  {
    case 0:
      self.tappedButtonText = @"ONE";
      break;
      
    case 1:
      self.tappedButtonText = @"TWO";
      break;
      
    default:
      self.tappedButtonText = @"THREE";
      break;
  }
  
  /* Perform custom segue to the previous view */
  [self performSegueWithIdentifier: kBackToFirstViewControllerSegueId
                            sender: sender];
}

@end
