//
//  ThirdViewController.m
//  MyAssessment
//
//  Created by Raymond Brion on 13/04/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import "ThirdViewController.h"

@interface ThirdViewController ()

@property (nonatomic, strong) NSDateFormatter* dateFormatter;
@property (nonatomic, strong) NSCalendar* calendar;

@end

@implementation ThirdViewController

#pragma mark - View lifecycle

- (void) viewDidLoad
{
  [super viewDidLoad];
  
  /* Perform setup */
  
  [self setupDateFormatter];
  [self setupCalendar];
}

#pragma mark - Setup and invalidation methods

- (void) setupDateFormatter
{
  self.dateFormatter = [NSDateFormatter new];
  [self.dateFormatter setDateFormat: @"dd MMMM YYYY"];
}

- (void) invalidateDateFormatter
{
  self.dateFormatter = nil;
}

- (void) setupCalendar
{
  self.calendar = [NSCalendar currentCalendar];
}

- (void) invalidateCalendar
{
  self.calendar = nil;
}

#pragma mark - UITableViewDataSource methods

- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
  return 1;
}

- (NSInteger) tableView: (UITableView*) tableView
  numberOfRowsInSection: (NSInteger)    section
{
  return 12;
}


- (UITableViewCell*) tableView: (UITableView*) tableView
         cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
  NSString* reuseIdentifier = @"dateCell";
  
  UITableViewCell* cell
    = [tableView dequeueReusableCellWithIdentifier: reuseIdentifier];
  
  if (cell == nil)
  {
    cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleValue1
                                  reuseIdentifier: reuseIdentifier];
  }
  
  /* Set the text based on the date */

  NSDateComponents* oneDay = [[NSDateComponents alloc] init];
  [oneDay setDay: indexPath.row];
  
  NSDate* adjustedDate
    = [self.calendar dateByAddingComponents: oneDay
                                     toDate: [NSDate date]
                                    options: NSCalendarWrapComponents];
  
  NSString* text = [self.dateFormatter stringFromDate: adjustedDate];
  
  cell.textLabel.text = text;
  
  return cell;
}

@end
