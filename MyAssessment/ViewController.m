//
//  ViewController.m
//  MyAssessment
//
//  Created by Raymond Brion on 13/04/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

static NSString* kToSecondViewControllerSegueID
  = @"toSecondViewController";

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UILabel* selectedLabel;
@property (nonatomic, strong) UIColor* selectedColor;

@end

@implementation ViewController

#pragma mark - View lifecycle

- (void) viewDidLoad
{
  [super viewDidLoad];
  
  self.tappedButtonText = nil;
  
  [self updateLabelText];
}

- (void) viewWillAppear: (BOOL) animated
{
  [self updateLabelText];
}

#pragma mark - Private methods

- (void) updateLabelText
{
  if (self.tappedButtonText == nil)
  {
    self.selectedLabel.text = @"None";
  }
  else
  {
    NSString* formattedString
      = [NSString stringWithFormat:
         @"button pressed is %@", self.tappedButtonText];
    self.selectedLabel.text = formattedString;
  }
}

#pragma mark - Navigation methods

- (IBAction) unwindToFirstViewController: (UIStoryboardSegue*) unwindSegue
{
  [self updateLabelText];
}

- (void) prepareForSegue: (UIStoryboardSegue*) segue
                  sender: (id)                 sender
{
  if ([segue.identifier isEqualToString: kToSecondViewControllerSegueID])
  {
    SecondViewController* viewController
      = (SecondViewController*) segue.destinationViewController;
    
    viewController.backgroundColor = self.selectedColor;
    
    self.tappedButtonText = nil;
  }
}

#pragma mark - IBAction methods

- (IBAction) buttonTapped: (id) sender
{
  /* Determine the color based on the button's tag */
  UIButton* button = (UIButton*) sender;
  switch (button.tag)
  {
    case 0:
      self.selectedColor = [UIColor blueColor];
      break;
      
    case 1:
      self.selectedColor = [UIColor redColor];
      break;
      
    default:
      self.selectedColor = [UIColor greenColor];
      break;
  }
  
  /* Perform custom segue to the next view */
  [self performSegueWithIdentifier: kToSecondViewControllerSegueID
                            sender: sender];
}

@end
