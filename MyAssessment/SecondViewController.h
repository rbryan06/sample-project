//
//  SecondViewController.h
//  MyAssessment
//
//  Created by Raymond Brion on 13/04/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (nonatomic, strong) UIColor* backgroundColor;

@end
