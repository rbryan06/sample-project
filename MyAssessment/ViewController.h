//
//  ViewController.h
//  MyAssessment
//
//  Created by Raymond Brion on 13/04/15.
//  Copyright (c) 2015 RaymondBrion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) NSString* tappedButtonText;

@end

